var browserify = require('browserify');
var gulp = require('gulp');
var browserSync  = require('browser-sync');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var gutil = require('gulp-util');
var eslint = require('gulp-eslint');
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var cssnano = require('gulp-cssnano');


// error reporter
var reportError = function (error) {
  gutil.beep();
  gutil.log(gutil.colors.red('Error: ' + error.message));
}

var isBuild = function(e){
  return e = 'build';
}

// Browser sync
gulp.task('browserSync', function() {
  browserSync({
    // url setup in mamp
    proxy: 'kirbysite.dev',
    // launch external url
    open: 'external',
  })
})

// compile scss
gulp.task('styles', function(){
  gutil.log(gutil.colors.green('Bundling styles...'));
  return gulp.src('assets/styles/scss/*.scss')
    .pipe(sass())
    .on('error', reportError)
    .pipe(autoprefixer())
    .pipe(this.seq.includes('build') ? cssnano({zindex: false}) : gutil.noop()) // if build task: minify but dont mess with my z-indexes
    .pipe(gulp.dest('assets/styles/css'))
    .pipe(browserSync.reload({ stream: true }));
});

gulp.task('lint', function(){
  return gulp.src(['assets/js/**/*.js', '!assets/js/dist', '!assets/js/dist/**'])
    .pipe(eslint({
        'extends': "eslint:recommended",
        'rules': {
          'indent': [1, 2],
          'no-console': 0
        },
        'env': {
          'commonjs': true
        }
    }))
    .pipe(eslint.format())
    .pipe(eslint.failAfterError())
    .on('error', reportError);
});

gulp.task('scripts', ['lint'], function(){
  gutil.log(gutil.colors.green('Bundling scripts...'));
  browserify('./assets/js/main.js',{ debug: true })
    .bundle() // create text tream
    .on('error', reportError)
    .pipe(source('app.js'))// convert text streams from .bundle() to vinyl streams
    .pipe(buffer()) // convert streaming vinyl files to use buffer
    .pipe(this.seq.includes('build') ? uglify() : gutil.noop()) // if build task: minify js
    .pipe(gulp.dest('./assets/js/dist'));
});

// make sure scripts are complied before hard reloading browsers
gulp.task('scripts-watch', ['scripts'], browserSync.reload);

// build everything
gulp.task('build', ['scripts', 'styles']);

// default task watching
gulp.task('default',  ['browserSync', 'styles', 'scripts'], function(){
  gulp.watch('assets/js/*.js', ['scripts-watch'] );
  gulp.watch('assets/styles/scss/*.scss', ['styles']);
});