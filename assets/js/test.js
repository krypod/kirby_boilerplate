var hello = function() {
  console.log('hello');
}

var world = function() {
  console.log('world');
}

exports.hello = hello;
exports.world = world;