<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <title><?php echo $site->title()->html() ?> | <?php echo $page->title()->html() ?></title>
  <meta name="description" content="<?php echo $site->description()->html() ?>">
  <?php ecco( c::get('robots') === false, '<meta name="robots" content="noindex, nofollow" />') ?> 
  <?php echo css('assets/styles/css/main.css') ?>
</head>
<body>